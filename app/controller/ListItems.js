Ext.define('ShoppingList.controller.ListItems', {
    extend: 'Ext.app.Controller',

    requires: [
    ],

    config: {

        models: [
            'ShoppingList.model.ListItem'
        ],

        stores: [
            'ShoppingList.store.ListItems'
        ],

        refs: {
            saveListBtn: '#mainTitleBar button[action=savelist]',
            backTitleBarBtn: '#mainTitleBar button[action=backtolists]',
            main: 'main',
            listItems: 'listitems',
            list: 'listitems list',
            startScanBtn: '#startScanBtn',
            listName: '#listName',
            listItemsTotalPrice: '#listItemsTotalPrice'
        },

        control: {
            saveListBtn: {
                tap: 'onSaveListBtnTap'
            },
            backTitleBarBtn: {
                tap: 'onBackTitleBarBtnTap'
            },
            listItems: {
                show: 'onListItemsShow'
            },
            startScanBtn: {
                tap: 'startScan'
            },
            list: {
                itemswipe: 'onListItemSwipe',
                itemtaphold: 'onListItemTapHold'
            }
        }
    },

    onListItemsShow: function(self, e) {
        var me = this,
            store = Ext.getStore('listitemsstore'),
            list = me.getList(),
            listItemsView = me.getListItems(),
            saveListBtn = me.getSaveListBtn(),
            startScanBtn = me.getStartScanBtn(),
            listName = me.getListName();

        listName.setHtml(listItemsView.parentList.name);

        store.clearFilter();

        store.load({
            callback: function(records, operation, success) {
                if(success) {
                    list.unmask();
                    saveListBtn.enable();
                    startScanBtn.enable();
                }
                else {
                    Ext.Msg.alert('Pogreška', 'Neuspjelo učitavanje popisa', function(btn) {
                        me.onBackTitleBarBtnTap(me, null);
                    });
                }
            },
            scope: this
        });
        store.filter('list', listItemsView.parentList.id);
        me.refreshPrice();

        listName.element.on('tap', me.onListNameTap, me);
    },

    onListNameTap: function(self, e) {
        var me = this,
            currentRecord = me.getListItems().parentList,
            store = Ext.getStore('listsstore'),
            listName = me.getListName();

        Ext.Msg.prompt('Izmjena popisa', 'Unesite novi naziv popisa', function(btn, text) {
            if(!Ext.isEmpty(text) && btn === 'ok') {
                var record = store.getById(currentRecord.id);

                record.set('name', text);
                store.sync();
                listName.setHtml(text);
            }
        });
    },

    onBackTitleBarBtnTap: function(self, e) {
        var main = this.getMain(),
            itemIndex = main.itemIndex,
            animation = { type: 'slide', direction: 'right' };

        main.animateActiveItem(itemIndex['lists'], animation);
    },

    onSaveListBtnTap: function(self, e) {
        var store = Ext.getStore('listitemsstore');
        store.sync();
        this.onBackTitleBarBtnTap(this, null);
    },

    startScan: function(self, e) {
        var me = this;
        cordova.plugins.barcodeScanner.scan(
            function(result) {
                if(!result.cancelled)
                    me.scanSuccessful(result);
            },
            function(error) {
                me.scanError(error);
            }
        );
        //me.scanSuccessful({text: 'Proslo;' + Math.random()});
    },

    scanSuccessful: function (result) {
        var me = this,
            store = Ext.getStore('listitemsstore'),
            listItems = me.getListItems(),
            listParam = listItems.currentList,
            newRecord = null,
            name = "",
            price = 0;

        var d = result.text.split(";");

        if (d.length == 2) {

            name = d[0];
            if(name.length === 0)
                return;

            price = Number(d[1]);
            if(isNaN(price))
                return;

            newRecord = Ext.create('ShoppingList.model.ListItem', {
                name: name,
                list: listItems.parentList.id,
                price: price
            });

            store.add(newRecord);
            me.refreshPrice();
        }
    },

    scanError: function (error) {
        Ext.Msg.alert("Pogreška u skeniranju", error);
    },

    onListItemSwipe: function(self, index, target, record, e) {
        var me = this,
            quantity = record.get('quantity'),
            store = Ext.getStore('listitemsstore');
        if(e.direction == 'right') {
            quantity++;
        } else if (e.direction == 'left') {
            if(quantity === 1) {
                store.remove(record);
            } else {
                quantity--;
            }
        }
        record.set('quantity', quantity);
        me.refreshPrice();
    },

    refreshPrice: function() {
        var me = this,
            total = me.getListItemsTotalPrice(),
            store = Ext.getStore('listitemsstore'),
            sum = 0;

        Ext.each(store.data.items, function(rec) {
            sum += rec.get('quantity') * rec.get('price');
        });

        total.setHtml(sum.toFixed(2));
    },

    onListItemTapHold: function(self, index, target, record, e) {
        var me = this,
            store = Ext.getStore('listitemsstore'),
            name = record.get('name');

        Ext.Msg.confirm("Potvrda", "Želite li obrisati artikl '" + name + "'?", function(btn){
            if(btn === 'yes') {
                store.remove(record);
                me.refreshPrice();
            }
        });
    }
});
