Ext.define('ShoppingList.controller.Main', {
    extend: 'Ext.app.Controller',

    requires: [
        'Ext.data.proxy.LocalStorage'
    ],

    config: {

        stores: ['ShoppingList.store.ListItems'],

        models: ['ShoppingList.model.ListItem'],

        refs: {
            titlebar: '#mainTitleBar'
        },

        control: {
            'main container': {
                show: 'onContainerShow'
            }
        }
    },

    onContainerShow: function(self) {
        var me = this,
            titlebar = me.getTitlebar(),
            buttons = titlebar.query('button'),
            newButtons = self['buttons'];

        Ext.each(buttons, function(btn) {
            btn.destroy();
        });

        Ext.each(newButtons, function(btn) {
            titlebar.add(btn);
        });
    }
});
