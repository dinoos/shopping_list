Ext.define('ShoppingList.controller.StartScreen', {
    extend: 'Ext.app.Controller',

    requires: [
    ],

    config: {

        refs: {
            startAppImg: '#startAppImg',
            main: 'main'
        },

        control: {
            startAppImg: {
                tap: 'onStartAppImgTap'
            }
        }
    },

    onStartAppImgTap: function(self, e) {
        var main = this.getMain(),
            itemIndex = main.itemIndex,
            animation = { type: 'pop' };
        main.animateActiveItem(itemIndex['mainmenu'], animation);
    }
});
