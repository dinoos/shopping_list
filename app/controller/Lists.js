Ext.define('ShoppingList.controller.Lists', {
    extend: 'Ext.app.Controller',

    requires: [
    ],

    config: {

        models: [
            'ShoppingList.model.List'
        ],

        stores: [
            'ShoppingList.store.Lists'
        ],

        refs: {
            newListTitleBarBtn: '#mainTitleBar button[action=addlist]',
            backTitleBarBtn: '#mainTitleBar button[action=backtomainmenu]',
            main: 'main',
            mainTitleBar: '#mainTitleBar',
            lists: 'lists',
            list: 'lists list'
        },

        control: {
            newListTitleBarBtn: {
                tap: 'onNewListTitleBarBtnTap'
            },
            backTitleBarBtn: {
                tap: 'onBackTitleBarBtnTap'
            },
            lists: {
                show: 'onListsShow'
            },
            list: {
                itemtap: 'onListItemTap',
                itemtaphold: 'onListItemTapHold'
            }
        }
    },

    onListItemTapHold: function(self, index, target, record, e) {
        var me = this,
            store = Ext.getStore('listsstore'),
            listItemsStore = Ext.getStore('listitemsstore'),
            name = record.get('name'),
            list = me.getList();

        list.suspendEvents();

        Ext.Msg.confirm("Potvrda", "Želite li obrisati listu '" + name + "'?", function(btn){
            if(btn === 'yes') {
                store.remove(record);
                me.clearListItemsStore(listItemsStore, record);
                store.sync();
            }
            list.resumeEvents(true);
        });
    },

    clearListItemsStore: function(store, record) {
        store.clearFilter();
        store.load();
        store.filter('list', record.get('id'));
        var forDelete = [];
        Ext.each(store.data.items, function(rec) {
            forDelete.push(rec);
        });
        Ext.each(forDelete, function(rec) {
            store.remove(rec);
        });
        store.sync();
    },

    onListItemTap: function(self, index, target, record, e) {
        var main = this.getMain(),
            itemIndex = main.itemIndex,
            listitems = main.child('listitems'),
            listItemsIndex = itemIndex['listitems'],
            animation = { type: 'slide', direction: 'left' };

        listitems.parentList = {
            id: record.get('id'),
            name: record.get('name')
        };
        main.animateActiveItem(listItemsIndex, animation);
    },

    onListsShow: function(self, e) {
        var me = this,
            store = Ext.getStore('listsstore'),
            list = me.getList(),
            newListTitleBarBtn = this.getNewListTitleBarBtn(),
            main = this.getMain();

        store.load({
            callback: function(records, operation, success) {
                if(success) {
                    list.unmask();
                    newListTitleBarBtn.enable();
                }
                else {
                    Ext.Msg.alert('Pogreška', 'Neuspjelo učitavanje popisa', function(btn) {
                        me.onBackTitleBarBtnTap(me, null);
                    });
                }
            },
            scope: this
        });
    },

    onBackTitleBarBtnTap: function(self, e) {
        var main = this.getMain(),
            itemIndex = main.itemIndex,
            animation = { type: 'slide', direction: 'right' };

        main.animateActiveItem(itemIndex['mainmenu'], animation);
    },

    onNewListTitleBarBtnTap: function(self, e) {
        Ext.Msg.prompt('Novi popis', 'Unesite naziv novog popisa', function(btn, text) {
            if(!Ext.isEmpty(text) && btn === 'ok') {
                var list = Ext.create('ShoppingList.model.List', {
                        name: text,
                        date: new Date()
                    }),
                    store = Ext.getStore('listsstore');

                store.add(list);
                store.data.items[0].setDirty();
                store.sync();
            }
        });
    }
});
