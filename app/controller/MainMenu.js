Ext.define('ShoppingList.controller.MainMenu', {
    extend: 'Ext.app.Controller',

    requires: [
    ],

    config: {

        refs: {
            showListsBtn: '#showListsBtn',
            newListBtn: '#newListBtn',
            main: 'main'
        },

        control: {
            showListsBtn: {
                tap: 'onShowListsBtnTap'
            },
            newListBtn: {
                tap: 'onNewListBtnTap'
            }
        }
    },

    onShowListsBtnTap: function(self, e) {
        var main = this.getMain(),
            itemIndex = main.itemIndex,
            animation = { type: 'slide', direction: 'left' };

        main.animateActiveItem(itemIndex['lists'], animation);
    },

    onNewListBtnTap: function(self, e) {
        var me = this;
        Ext.Msg.prompt('Novi popis', 'Unesite naziv novog popisa', function(btn, text) {
            if(!Ext.isEmpty(text) && btn === 'ok') {
                var list = Ext.create('ShoppingList.model.List', {
                        name: text,
                        date: new Date()
                    }),
                    store = Ext.getStore('listsstore');

                store.add(list);
                store.data.items[0].setDirty();
                store.sync();

                var main = me.getMain(),
                    itemIndex = main.itemIndex,
                    listitems = main.child('listitems'),
                    listItemsIndex = itemIndex['listitems'],
                    animation = { type: 'slide', direction: 'left' };

                listitems.parentList = {
                    id: list.get('id'),
                    name: list.get('name')
                };
                main.animateActiveItem(listItemsIndex, animation);
            }
        });
    }
});
