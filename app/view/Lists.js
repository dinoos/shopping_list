Ext.define('ShoppingList.view.Lists', {
    extend: 'Ext.Container',
    xtype: 'lists',

    requires: [
        'Ext.Img',
        'Ext.Label',
        'Ext.dataview.List'
    ],

    config: {
        layout: 'fit',
        cls: 'no__bg',

        items: [
            {
                xtype: 'list',
                id: 'shoppinglists',
                emptyText: 'Niste napravili niti jedan popis za kupnju',
                itemTpl: '<div class="lists__listitem">{name} <span>{date:date("d.m.Y")}</span></div>',
                masked: {
                    xtype: 'loadmask',
                    message: 'Učitavanje... '
                },
                store: 'listsstore',
                cls: 'no__bg'
            }
        ]
    },

    buttons: [
        {
            iconCls: 'home',
            iconMask: true,
            ui: 'plain',
            align: 'left',
            action: 'backtomainmenu'
        },
        {
            iconCls: 'add',
            ui: 'plain',
            iconMask: true,
            align: 'right',
            action: 'addlist',
            disabled: true
        }
    ]
});
