Ext.define('ShoppingList.view.ListItems', {
    extend: 'Ext.Container',
    xtype: 'listitems',

    requires: [
        'Ext.Img',
        'Ext.Label',
        'Ext.dataview.List'
    ],

    config: {
        layout: 'fit',

        items: [
            {
                layout: {
                    type: 'hbox',
                    pack: 'left',
                    align: 'middle'
                },
                flex: 1,
                docked: 'top',
                padding: "15px",
                items: [
                    {
                        xtype: 'label',
                        id: 'listName'
                    }
                ]
            },
            {
                xtype: 'list',
                id: 'listitemslist',
                emptyText: 'Popis nema niti jedan artikl',
                /* jshint multistr: true */
                itemTpl: new Ext.XTemplate(
                     '<div class="listitem__name">{name}</div>\
                      <div class="listitem__quantity">{quantity}</div>\
                      <div class="listitem__price">{[this.currencyFormat(values.price, values.quantity)]}</div>', {
                        currencyFormat : function(price, quantity) {
                          return (price*quantity).toFixed(2);
                      }
                }),
                masked: {
                    xtype: 'loadmask',
                    message: 'Učitavanje... '
                },
                store: 'listitemsstore',
                cls: 'listitems'
            },
            {
                layout: {
                    type: 'vbox',
                    pack: 'center',
                    align: 'middle'
                },
                flex: 1,
                docked: 'bottom',
                padding: 10,
                items: [
                    {
                        xtype: 'button',
                        iconCls: 'search',
                        iconMask: 'true',
                        text: 'Dodaj artikl',
                        id: 'startScanBtn',
                        disabled: true,
                        ui: 'plain'
                    }
                ]
            },
            {
                layout: {
                    type: 'hbox',
                    pack: 'center',
                    align: 'right'
                },
                docked: 'bottom',
                cls: 'listitem__total',
                items: [
                    {
                        xtype: 'label',
                        html: '127.35',
                        docked: 'right',
                        cls: 'listitem__total--price',
                        padding: 10,
                        itemId: 'listItemsTotalPrice'
                    },
                    {
                        xtype: 'label',
                        html: 'Ukupno:',
                        docked: 'right',
                        cls: 'listitem__total--label',
                        padding: 10
                    }
                ]
            }
        ]
    },

    buttons: [
        {
            ui: 'plain',
            iconCls: 'list',
            iconMask: true,
            align: 'left',
            action: 'backtolists'
        },
        {
            iconCls: 'check2',
            iconMask: true,
            ui: 'plain',
            align: 'right',
            action: 'savelist',
            disabled: true
        }
    ]
});
