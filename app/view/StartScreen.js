Ext.define('ShoppingList.view.StartScreen', {
    extend: 'Ext.Container',
    xtype: 'startscreen',

    requires: [
        'Ext.Img',
        'Ext.Label'
    ],

    config: {
        layout: 'fit',
        cls: 'no__bg',

        items: [
            {
                layout: {
                    type: 'vbox',
                    pack: 'center',
                    align: 'middle'
                },
                items: [
                    {
                        xtype: 'label',
                        /* jshint multistr: true */
                        html: '<center class="welcome">DOBRODOŠLI</center>\
                                <center class="welcome__small">za početak kliknite na zeleni gumb</center>'
                    },
                    {
                        xtype: 'image',
                        id: 'startAppImg',
                        margin: '50px 0 0 0',
                        src: 'resources/icons/Icon.png',
                        width: 140,
                        height: 140
                    }
                ]
            }
        ]
    }
});
