Ext.define('ShoppingList.view.MainMenu', {
    extend: 'Ext.Container',
    xtype: 'mainmenu',

    requires: [
        'Ext.Img'
    ],

    config: {
        layout: 'vbox',
        cls: 'no__bg',

        items: [
            {
                layout: {
                    type: 'hbox',
                    pack: 'center',
                    align: 'middle'
                },
                flex: 1,
                items: [
                    {
                        xtype: 'button',
                        ui: 'plain',
                        id: 'newListBtn',
                        /* jshint multistr: true */
                        html: '<center class="red">SKENIRAJ</center>\
                                <center class="green">artikle</center>',
                        width: 200,
                        height: 100,
                        iconAlign: 'top',
                        iconCls: 'add',
                        iconMask: true
                    }
                ]
            },
            {
                layout: {
                    type: 'hbox',
                    pack: 'center',
                    align: 'middle'
                },
                flex: 1,
                items: [
                    {
                        xtype: 'button',
                        ui: 'plain',
                        id: 'showListsBtn',
                        /* jshint multistr: true */
                        html: '<center class="red">MOJI POPISI</center>\
                                <center class="green">za kupnju</center>',
                        width: 200,
                        height: 100,
                        iconAlign: 'top',
                        iconCls: 'list',
                        iconMask: true
                    }
                ]
            }
        ]
    }
});
