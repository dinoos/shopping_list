Ext.define('ShoppingList.view.Main', {
    extend: 'Ext.Container',
    xtype: 'main',

    requires: [
        'Ext.Img',
        'Ext.Label',
        'Ext.TitleBar'
    ],

    config: {
        layout: 'card',
        cls: 'main__bg',

        items: [
            {
                xtype: 'titlebar',
                id: 'mainTitleBar',
                docked: 'top',
                layout: {
                    pack: 'center'
                },
                centered: true,
                title: '<div class="title">m-Košarica</div>',
                items: [
                ],
                ui: 'maintitlebar'
            },
            { xclass: 'ShoppingList.view.StartScreen' },
            { xclass: 'ShoppingList.view.MainMenu' },
            { xclass: 'ShoppingList.view.Lists' },
            { xclass: 'ShoppingList.view.ListItems' }
        ]
    },

    itemIndex: {
        'startscreen': 0,
        'mainmenu': 1,
        'lists': 2,
        'listitems': 3
    }
});
