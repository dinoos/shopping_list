Ext.define('ShoppingList.store.Lists', {
    extend: 'Ext.data.Store',
    alias: 'store.Lists',

    requires: [
        'ShoppingList.model.List'
    ],

    config: {
        model: 'ShoppingList.model.List',
        storeId: 'listsstore',
        autoLoad: false,
        proxy: {
            type: 'localstorage',
            id: 'listsstoreproxy'
        }
    }
});