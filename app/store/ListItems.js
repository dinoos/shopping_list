Ext.define('ShoppingList.store.ListItems', {
    extend: 'Ext.data.Store',
    alias: 'store.ListItems',

    requires: [
        'ShoppingList.model.ListItem'
    ],

    config: {
        model: 'ShoppingList.model.ListItem',
        storeId: 'listitemsstore',
        autoLoad: false,
        proxy: {
            type: 'localstorage',
            id: 'listitemsstoreproxy'
        }
    }
});