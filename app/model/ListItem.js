Ext.define('ShoppingList.model.ListItem', {
    extend: 'Ext.data.Model',
    alias: 'model.ListItem',

    config: {
        identifier: 'uuid',
        fields: [
            { name: 'name', type: 'string' },
            { name: 'price', type: 'number', defaultValue: 0 },
            { name: 'quantity', type: 'int', defaultValue: 1 },
            { name: 'list' }
        ],
        validations: [
            {
                type: 'presence',
                field: 'name',
                message: 'Naziv artikla je obavezan'
            }
        ]
    }
});