Ext.define('ShoppingList.model.List',{
    extend: 'Ext.data.Model',
    alias: 'model.List',

    config: {
        identifier:'uuid',
        fields: [
            { name: 'id', type: 'int' },
            { name: 'name', type: 'string' },
            { name: 'date', type: 'date' },
            { name: 'ordered', type: 'boolean', defaultValue: false }
        ],
        validations: [
            {
                type: 'presence',
                field: 'name',
                message: 'Naziv liste je obavezan'
            },
            {
                type: 'presence',
                field: 'date'
            }
        ]
    }
});